num = int(input())
if num < 2:
    print("Число должно быть больше 1")
else:
    is_prime = True
    for i in range(2, num):
        if num % i == 0:
            is_prime = False
            break
    if is_prime:
        print("Простое")
    else:
        print("Составное")
